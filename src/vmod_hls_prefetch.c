#include "config.h"
#include "cache/cache.h"

#include <stdio.h>
#include <string.h>
#include "vtim.h"
#include <vcl.h>
#include "vcc_hls_prefetch_if.h"


int
init_function(struct vmod_priv *priv, const struct VCL_conf *conf)
{
	// Initialization code, if any, goes here
	return (0);
}

// VCL_STRING
// vmod_segment_urls(const struct vrt_ctx *ctx, VCL_STRING hls_manifest)
// {
// 	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
// 	AN(hls_manifest);

// 	const char *start = hls_manifest;
// 	const char *end;

// 	while ((end = strstr(start, "\n"))) {
// 		VSL(SLT_VCL_Log, ctx->req->sp->vxid, "HLS Segment URL: %.*s", (int)(end - start), start);
// 		start = end + 1;
// 	}

// 	if (*start) {
// 		VSL(SLT_VCL_Log, ctx->req->sp->vxid, "HLS Segment URL: %s", start);
// 	}

// 	return hls_manifest;
// }

VCL_STRING
vmod_segment_urls(const struct vrt_ctx *ctx, VCL_BODY hls_manifest)
{
    CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
    AN(hls_manifest);

    const char *start = hls_manifest;
    const char *end;

    while ((end = strstr(start, "\n"))) {
        VSL(SLT_VCL_Log, VSL_ID(ctx->req), "HLS Segment URL: %.*s", (int)(end - start), start);
        start = end + 1;
    }

    if (*start) {
        VSL(SLT_VCL_Log, VSL_ID(ctx->req), "HLS Segment URL: %s", start);
    }

    return hls_manifest;
}
